# The Octave queueing toolbox

Copyright (C) 2008, 2009, 2010, 2011, 2012, 2013, 2018, 2020, 2024

Moreno Marzolla <moreno.marzolla (at) unibo.it>

## Description

The _queueing toolbox_ ("queueing", in short) is a collection of GNU
Octave scripts for numerical evaluation of queueing network
models. Open, closed and mixed networks are supported, with single or
multiple classes of customers. The queueing toolbox also provides
functions for steady-state and transient analysis of Markov chains, as
well as for single station queueing systems.

This package requires GNU Octave version 4.0.0 or later. GNU Octave is
free software and is available at https://www.gnu.org/software/octave/

## Download and install

The Web pages of the queueing toolbox are

https://gnu-octave.github.io/packages/queueing/

https://www.moreno.marzolla.name/software/queueing/

where the latest version can be downloaded. See the installation
instructions there.

## Citation

To cite the Octave queueing package use:

> Moreno Marzolla, The qnetworks Toolbox: A Software Package for
> Queueing Networks Analysis. Khalid Al-Begain, Dieter Fiems and
> William J. Knottenbelt, Editors, Proceedings 17th International
> Conference on Analytical and Stochastic Modeling Techniques and
> Applications (ASMTA 2010) Cardiff, UK, June 14--16, 2010, volume
> 6148 of Lecture Notes in Computer Science, Springer, pp. 102--116,
> ISBN 978-3-642-13567-5, DOI 10.1007/978-3-642-13568-2_8

A BibTeX entry for LaTeX users is:

```
@inproceedings{queueing,
    author    = {Moreno Marzolla},
    title     = {The qnetworks Toolbox: A Software Package for Queueing Networks Analysis},
    booktitle = {Analytical and Stochastic Modeling Techniques and Applications, 17th International Conference, ASMTA 2010, Cardiff, UK, June 14--16, 2010. Proceedings},
    editor    = {Khalid Al-Begain and Dieter Fiems and William J. Knottenbelt},
    year      = {2010},
    publisher = {Springer},
    series    = {Lecture Notes in Computer Science},
    volume    = {6148},
    pages     = {102--116},
    ee        = {http://dx.doi.org/10.1007/978-3-642-13568-2_8},
    doi       = {10.1007/978-3-642-13568-2_8},
    isbn      = {978-3-642-13567-5}
}
```

## Documentation

The queueing toolbox comes with extensive documentation; here is the
user manual in
[pdf](https://www.moreno.marzolla.name/software/queueing/queueing.pdf)
and
[html](https://www.moreno.marzolla.name/software/queueing/queueing.html)
format. Usage help for all functions is available through the `help`
built-in command at the GNU Octave prompt. See the manual for details.

The Octave queueing toolbox is distributed under the terms of the GNU
General Public License, version 3 or later. See the file <COPYING> for
details.
